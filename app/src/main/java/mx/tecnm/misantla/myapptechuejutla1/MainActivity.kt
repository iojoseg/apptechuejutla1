package mx.tecnm.misantla.myapptechuejutla1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_Procesar.setOnClickListener {

          var nombre = edt_Nombre.text.toString()
           var apellido = edt_Apellidos.text.toString()

            val nombrecompleto = "$nombre $apellido"
            tv_resultado.text = nombrecompleto

        }
        
    }
}